import os

__author__ = 'GANGESHWAR'
APP_DEBUG = True
SECRET_KEY = '\xbf\xa3\xdc\xcd\xabI2O\xad\xa2Nd\x0c\xd6\t\x80P.d&\x0cc4v'

basedir = os.path.abspath(os.path.dirname(__file__))

# TODO: Change this variable before and after deploying to GAE
"""
environment type -
    0 for localhost ( testing )
    1 for Google App Engine ( production )
"""
ENVIR = 0  # change according to the environment currently used

"""
SQLALCHEMY_DATABASE_URI - URI of the database
    localhost: mysql://<username>:<password>@localhost:3306/<dbname>
    GAE: mysql+mysqldb://root@/<dbname>?unix_socket=/cloudsql/<projectid>:<instancename>
"""
if ENVIR:
    SQLALCHEMY_DATABASE_URI = 'mysql+mysqldb://root@/grinionsdb?unix_socket=/cloudsql/grinions-1:grinions-db'

else:
    SQLALCHEMY_DATABASE_URI = 'mysql://root:A0Lo+))22@localhost:3306/grinions'
