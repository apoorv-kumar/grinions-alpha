import os
import sys

from main import app

sys.path.insert(1, os.path.join(os.path.abspath('.'), 'lib'))

if __name__ == '__main__':
    app.run(debug=True)
