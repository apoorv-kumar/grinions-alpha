import operator
import os
import random
import string
from datetime import datetime

from flask import render_template, send_from_directory, g, session as login_session
from flask import request, url_for, redirect
from flask.ext.login import logout_user, login_user, current_user, login_required
from main import app, db, login_manager
from main.models import User, Event, Invitee, Contribution
from utils import createUser, userInfoByEmail

login_manager.login_view = 'login'
login_manager.session_protection = 'strong'


# Flask-Login use this to reload the user object from the user ID stored in the session
@login_manager.user_loader
def load_user(id):

    return User.query.get(int(id))


@app.before_request
def before_request():
    g.user = current_user


############# VIEWS ################

# test page
@app.route('/grin_event')
def test_event():
    return render_template('grinions-create-event.html'
                           )


# endpoint to reset the application
@app.route('/reset')
def resetapp():
    try:
        logout_user()
        login_session.clear()
    except:
        pass

    db.drop_all()
    db.create_all()
    return redirect(url_for('index'))


# endpoint to reset database
@app.route('/resetdb')
def resetdb():
    db.drop_all()
    db.create_all()
    return """Reset done</br>
        <a href= "/testdb">Test db</a>
    """


# endpoint to test database
@app.route("/testdb")
def testdb():
    u = User("admin@grinions.com", "admin", "Administrator", "", "M")
    try:
        db.session.add(u)
        return render_template('test_db.html', title='Success! DB working fine')
    except Exception as e:
        return render_template('test_db.html', title='Fail! Error in inserting object to DB')


# index view
@app.route('/index')
@app.route('/')
def index():
    nxt = request.args.get('next')
    print(nxt)
    if not g.user.is_authenticated:
        state = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(32))
        login_session['state'] = state
        return render_template('grinions-welcome.html', STATE=state, redir_url=nxt)
    return render_template('grinions-welcome.html', user=g.user)


# index view
@app.route('/login1')
def login1():
    nxt = request.args.get('next')
    print(nxt)
    if not g.user.is_authenticated:
        state = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(32))
        login_session['state'] = state
        return render_template('login.html', STATE=state, redir_url=nxt)
    return render_template('index.html', user=g.user)


@app.route('/logout')
def logout():
    logout_user()
    try:
        login_session.clear()
    except:
        pass
    return redirect(url_for('index'))


@app.route('/signup')
def signup():
    nxt = request.args.get('next')
    print(nxt)
    if not g.user.is_authenticated:
        state = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(32))
        login_session['state'] = state
        return render_template('signup.html', STATE=state, redir_url=nxt)
    # return render_template('index.html', user=g.user)
    return redirect(nxt or url_for('index'))


@app.route('/home')
@login_required
def home():
    # del login_session['logged_in']
    return redirect(url_for('index'))


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico',
                               mimetype='image/vnd.microsoft.icon')


@app.route('/test')
def test():
    return render_template('test.html')


# login view
@app.route('/login')
def login_view():
    nxt = request.args.get('next')
    if not g.user.is_authenticated:
        state = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(32))
        login_session['state'] = state
        return render_template('grinions-login.html', STATE=state, redir_url=nxt)
    return redirect(nxt or url_for('index'))


# VIEW and API for create password
@app.route('/create_password', methods=['POST', 'GET'])
def create_password():
    nxt = request.args.get('next')
    print(nxt)
    # if url was directly accessed without signing up
    if 'email' not in login_session:
        return redirect(url_for('index'))
    if userInfoByEmail(login_session['email']):
        return redirect(url_for('index'))
    if request.method == 'GET':
        return render_template('create_password.html')
    elif request.method == 'POST':
        password = str(request.form['new_password'])
        rpassword = str(request.form['conf_password'])
        if password != rpassword:
            return render_template('create_password.html', error="Passwords do not match.")
        else:
            user = createUser(login_session, password)
            login_user(user, remember=False)
            return redirect(nxt or url_for('index'))


# create event page
@app.route('/event/create')
#@login_required
def view_create_event():
    if current_user.is_authenticated:
     return render_template('grinions-create-event.html', user=g.user)
    else:
     return render_template('grinions-create-event.html')


# details of a particular event.
@app.route('/event/<int:eventid>')
def view_event(eventid):
    event = Event.query.filter_by(_id=eventid).first()
    if not event:
        return "Event not found"

    contributions = Contribution.query.filter_by(event_id=eventid).all()

    total_contribution = reduce(operator.add, map(lambda x: x.amount, contributions), 0)
    # TODO: SUPER INEFF right now. Do this in ORM SQL to make it efficient
    contributor_ids = map(lambda x: x.user_id, contributions)
    contributor_names = []
    for id in contributor_ids:
        contributor = User.query.filter_by(_id=id).first()
        name = contributor.first_name + " " + contributor.last_name
        contributor_names = contributor_names + [name]

    creator = User.query.filter_by(_id=event.creator_id).first()
    creator_name = creator.first_name + " " + creator.last_name

    time_progress = float((datetime.now().date() - event.start_date).days) / (event.ends_by - event.start_date).days
    event_stats = {"contributor_names": contributor_names,
                   "creator_name": creator_name,
                   "total_contribution": total_contribution,
                   "time_progress": time_progress}

    if (g.user is None) or (not g.user.is_authenticated):  # means anonymous user (not logged in)
        return render_template('grinions-event-page.html', event=event, event_stats=event_stats)
    else:

        contri = Contribution.query.filter_by(event_id=eventid, user_id=g.user.get_id()).first()
        if not contri:

            return render_template('grinions-event-page.html', event=event, user=g.user, event_stats=event_stats)
        else:

            return render_template('grinions-event-page.html', event=event, user=g.user, contributed=contri,
                                   event_stats=event_stats)


@app.route('/event/<int:eventid>/invitees')
def event_invitees(eventid):
    invitees = Invitee.query.filter_by(event_id=eventid).all()
    contributed = Contribution.query.filter_by(event_id=eventid).all()

    return


# all events created by the logged in user
@app.route('/me/events')
@login_required
def get_all_events_of_user():
    events = Event.query.filter_by(creator_id=g.user.get_id()).all()
    return render_template('grinions-user-events.html', events=events, user=g.user)


# public events by an user
@app.route('/user/<int:userid>/events/all')
@login_required
def get_all_events_of_a_user(userid):
    if userid == g.user._id:
        print(True)
        return redirect(url_for('get_all_events_of_user'))
    events = Event.query.filter_by(creator_id=userid).all()
    return render_template('grinions-user-events.html', events=events, user=g.user)


# list of all users
@app.route('/users/all')
@login_required
def all_users():
    users = User.query.all()
    print(users)
    return render_template('all_users.html', users=users, cuser=g.user)


# public profile of an user
@app.route('/user/<int:userid>')
@login_required
def user_profile(userid):
    if userid == g.user._id:
        print(True)
        return redirect(url_for('me'))

    is_me = False
    user = User.query.filter_by(_id=userid).first()
    if not user:
        return render_template('profile_not_exist.html')
    """
    if user.gender is None or user.gender == '':
        user.gender = 'Not set'
    if user.dob is None:
        user.dob = 'Not set'
    if user.country is None:
        user.country = 'Not set'
    """

    if is_me:
        events = Event.query.filter_by(creator_id=userid).all()
    else:
        events = Event.query.filter_by(creator_id=userid, private=False).all()

    return render_template('grinions-user-profile.html', events=events, cuser=g.user, user=user, is_me=is_me)


# public profile of the currently logged in user
@app.route('/me')
@login_required
def me():
    # anon users will be redirected to login page
    user = g.user
    """
    if user.gender is None or user.gender == '':
        user.gender = 'Not set'
    if user.dob is None:
        user.dob = 'Not set'
    if user.country is None:
        user.country = 'Not set'
    """

    events = Event.query.filter_by(creator_id=user.get_id()).all()
    return render_template('grinions-user-profile.html', events=events, cuser=user, user=user, is_me=True)


# not used
@app.route('/event/<int:eventid>/contribute', methods=['POST'])
def contribute(eventid):
    canContribute = True
    cuser = g.user
    event = Event.query.filter_by(_id=eventid).first()
    invitees = Invitee.query.filter_by(event_id=event.get_id())
    inviteeslist = []
    for i in invitees:
        inviteeslist.append(i.email_id)
    if (event.private) & (cuser.get_id() not in inviteeslist):
        canContribute = False
        return render_template('contribute.html', canContribute=canContribute)

    return render_template('contribute.html', canContribute=canContribute)


@app.route('/mail-test')
@login_required
def mail_temp():
    return render_template('mail.html', event_id=1, user=g.user, email_id=g.user.email, event_name='Test')


@app.route('/newsfeed')
@login_required
def newsfeed():
    events = Event.query.filter_by(private=False).all()
    return render_template("newsfeed.html", events=events,user=g.user)
