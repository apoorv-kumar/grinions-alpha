import json
import random
import string
from datetime import datetime

import httplib2
from google.appengine.api import mail

from flask import jsonify, request, url_for, redirect
from flask import make_response
from flask import render_template, g, session as login_session
from flask.ext.login import login_user, current_user,login_required
from main import app, db
from main.models import Event, Invitee, Contribution
from utils import createUser, getUserId, userInfo


@app.route('/api/v1/test')
def api_test():
    response = make_response(jsonify(response='API calls working', test='test'))
    response.headers['Content-Type'] = 'application/json'
    return response


#### LOGIN ####
@app.route('/fbconnect', methods=['POST'])
def fbconnect():
    if request.args.get('state') != login_session['state']:
        response = make_response(json.dumps('Invalid state parameter'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    access_token = request.data
    # app_id = json.loads(open(os.path.join(basedir, 'fb_client_secrets.json'), 'r').read())['web']['app_id']
    # app_secret = json.loads(open(os.path.join(basedir, 'fb_client_secrets.json'), 'r').read())['web']['app_secret']
    app_id = '219300215069594'
    app_secret = '17e937f8b3e3a00920ad05cde0601ee4'
    # url = 'https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id={0:s}&client_secret={1:s}&fb_exchange_token={2:s}&scope=\'email\''.format(
    # app_id, app_secret, access_token)
    url = """https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=%s&client_secret=%s&fb_exchange_token=%s""" % (
        app_id, app_secret, access_token)
    h = httplib2.Http()
    result = h.request(url, 'GET')[1]
    # use token info to get user info from API
    userinfo_url = "https://graph.facebook.com/v2.3/me"

    # strip expire tag from access token
    token = result.split("&")[0]
    url = 'https://graph.facebook.com/v2.3/me?%s' % token
    h = httplib2.Http()
    result = h.request(url, 'GET')[1]
    data = json.loads(result)
    print data

    if "email" not in data:
        response = make_response(json.dumps('Can\'t retreive your email address from Facebook.'), 401)
        response.headers['Content-Type'] = 'application/json'
        # return response
        login_session['email'] = data["id"] + '@facebook.com'  # temporary fix for getting email id from fb account.
    else:
        login_session['email'] = data["email"]

    login_session['fname'] = str(data["name"]).split(' ', 1)[0]
    login_session['lname'] = str(data["name"]).split(' ', 1)[1]
    login_session['facebook_id'] = data["id"]
    try:
        login_session['gender'] = data["gender"]
    except:
        pass
    # get user pic
    url = 'https://graph.facebook.com/v2.3/me/picture?%s&redirect=0' % token
    h = httplib2.Http()
    result = h.request(url, 'GET')[1]
    data = json.loads(result)
    print data
    login_session['picture'] = data["data"]["url"]
    login_session['logged_in'] = False

    return 'OK'


@app.route('/fbdisconnect')
def fbdisconnect():
    facebook_id = login_session['facebook_id']
    url = 'https://graph.facebook.com/%s/permissions' % facebook_id
    h = httplib2.Http()
    try:
        result = h.request(url, 'DELETE')[1]
    except:
        pass
    try:
        del login_session['facebook_id']
        return redirect(url_for('logout'))
    except:
        pass


@app.route('/signup/check', methods=['POST'])
def signup_check():
    try:
        name = str(request.form['name'])
        em = str(request.form['email'])
        pw = str(request.form['password'])
        conf_pw = str(request.form['conf_password'])
    except Exception, e:
        response = make_response(json.dumps("Request Error : " + e.message), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    if name == '' or name == ' ':
        response = make_response(json.dumps('Name field can\'t be empty'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    if em == '' or em == ' ':
        response = make_response(json.dumps('Email field can\'t be empty'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    if pw == '' or pw == ' ':
        response = make_response(json.dumps('Password field can\'t be empty'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    if conf_pw == '' or conf_pw == ' ':
        response = make_response(json.dumps('Confirm password field can\'t be empty'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    if pw != conf_pw:
        response = make_response(json.dumps('Passwords do not match'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response

    user_id = getUserId(em)
    if user_id is not None:
        response = make_response(json.dumps('An account with this email already exists.'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    fn = str(name).split(' ', 1)[0]
    try:
        ln = str(name).split(' ', 1)[1]
    except:
        ln = ''

    login_session['email'] = em
    login_session['fname'] = fn
    login_session['lname'] = ln
    newuser = createUser(login_session, pw)
    try:
        login_user(newuser, remember=False)
    except Exception, e:
        response = make_response(json.dumps('login_user Error: ' + e.message), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    response = make_response(json.dumps('New user created!'), 200)
    response.headers['Content-Type'] = 'application/json'
    return response


# checks login using form
@app.route('/auth/check', methods=['POST'])
def login_check():
    try:
        em = str(request.form['email'])
        pw = str(request.form['password'])
    except Exception, e:
        response = make_response(json.dumps('Requests error: ' + e.message), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    # rm = str(request.form['remember_me'])
    # print rm
    if em == '' or em == ' ':
        response = make_response(json.dumps('Email field can\'t be empty'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    if pw == '' or pw == ' ':
        response = make_response(json.dumps('Password field can\'t be empty'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    user_id = getUserId(em)
    if user_id is None:
        response = make_response(json.dumps('Email doesn\'t exist. Sign up now!'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    user = userInfo(user_id)
    if not user.verify_password(pw):
        response = make_response(json.dumps('Email and Password doesn\'t match.'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    try:
        login_user(user, remember=False)
    except Exception, e:
        response = make_response(json.dumps('login_user Error: ' + e.message), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    response = make_response(json.dumps('Login successful!'), 200)
    response.headers['Content-Type'] = 'application/json'
    return response


@app.route('/oauthlogin/check')
def oauthlogin_check():
    nxt = request.args.get('next')
    print(nxt)
    # check whether user exists, if not create new user
    user_id = getUserId(login_session['email'])
    if not user_id:
        return redirect(url_for('create_password', next=nxt))
    user = userInfo(user_id)
    login_user(user, remember=False)
    return redirect(nxt or url_for('index'))


@app.route('/auth')
def login():
    nxt = request.args.get('next')
    print(nxt)
    if not g.user.is_authenticated:
        state = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(32))
        login_session['state'] = state
        return render_template('grinions-login.html', STATE=state, redir_url=nxt)
    return redirect(nxt or url_for('view_create_event'))


# next or

@app.route('/need_auth',methods=['POST'])
def need_auth():
    login_session['data_avail'] = True
    login_session.update(dict(request.form.items()))
    response = make_response(redirect('/auth?next=/create_later'))
    #response.set_cookie('character', dict(request.form.items()))
    return response

@app.route('/create_later')
@login_required
def create_later():
    #try:
     #   data = json.loads(request.cookies.get('character'))
    #except TypeError:
     #   data = {}
    if login_session['data_avail']:
        return render_template('create_later.html', event_name=login_session.get("event_name"),
                                                event_desc=login_session.get("event_desc"),
                                                target_amt=login_session.get("target_amt"),
                                                contri_amt=login_session.get("contri_amt"),
                                                days=login_session.get("days-to-complete"))
    login_session['data_avail']=False
    return  render_template('/')


###### EVENTS #######
@app.route('/api/create_event', methods=['POST'])
def api_create_event():
    event_name = request.form.get('event_name')
    event_desc = request.form.get('event_desc')
    split_type = request.form.get('split_type')
    contri_amt = request.form.get('contri_amt')
    target_amt = request.form.get('target_amt')
    start_date = datetime.now()
    ends_by = float(request.form.get('ends_by'))
    private = request.form.get('isprivate')
    print event_name, event_desc, split_type, contri_amt, target_amt, ends_by, private
    if str(private) == 'true':
        private = True
    else:
        private = False
    print event_name, event_desc, split_type, contri_amt, target_amt, ends_by, private
    ends_by = datetime.fromtimestamp(ends_by)
    event = Event(g.user.get_id(), event_name, event_desc, split_type, contri_amt, target_amt, start_date, ends_by,
                  private)
    db.session.add(event)
    db.session.commit()  # after commit, the _id column will have the id of the object in the table
    print event.get_id()
    invitee = Invitee(email_id=g.user.email, event_id=event.get_id())
    db.session.add(invitee)
    db.session.commit()
    response = make_response(jsonify(event_id=event.get_id()))
    response.headers['Content-Type'] = 'application/json'

    return response


# Endpoint to create the databases
@app.route('/api/v1/create_db')
def create_db():
    db.drop_all()
    db.create_all()
    return 'db creation api'


@app.route('/api/invite', methods=['GET'])
def invite():
    if not g.user.is_authenticated:
        response = make_response(jsonify(result='User not authenticated to send mail.'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    user = g.user

    email_id = request.args.get('email_id')
    event_name = request.args.get('event_name')
    event_id = int(request.args.get('event_id'))
    if send_email(user, email_id, event_name, event_id):
        invitee = Invitee(event_id, email_id)
        db.session.add(invitee)
        db.session.commit()
        response = make_response(jsonify(result='Invited ' + email_id + ' to contribute'), 200)
        response.headers['Content-Type'] = 'application/json'
    else:
        response = make_response(jsonify(result='Couldn\'t invite ' + email_id), 401)
        response.headers['Content-Type'] = 'application/json'
    return response


def send_email(user, email_id, event_name, event_id):
    """
    @author: Gangeshwar
    currently the sent email goes to SPAM folder in Gmail.
    Link : https://support.google.com/mail/answer/81126?hl=en
    We have to use this guidelines to prevent that from happening

    Link : https://cloud.google.com/appengine/docs/python/mail/sendgrid
    Let's use SendGrid to send email from production app instead of native mail protocol like below.
    SendGrid has a flexibility of sending email from admin@grinions.com
    It has analytics too.
    """

    mess = mail.EmailMessage(sender=user.first_name + "<grinions-1@appspot.gserviceaccount.com>",
                             subject="You\'re invited to chip in for " + event_name + "")
    # html template
    mess.html = render_template('mail.html', event_id=event_id, user=user, email_id=email_id, event_name=event_name)
    mess.to = "<" + email_id + ">"

    try:
        mess.send()
        return True
    except Exception, e:
        print(e)
        return False


@app.route('/api/event/<int:eventid>/contribute', methods=['GET'])
def api_contribute(eventid):
    if not current_user.is_authenticated:
        response = make_response(jsonify(error='User not authenticated.'), 401)
        return response
    amount = request.args.get('amount')
    cuser = g.user
    event = Event.query.filter_by(_id=eventid).first()
    invitees = Invitee.query.filter_by(event_id=event.get_id())
    inviteeslist = []
    for i in invitees:
        inviteeslist.append(i.email_id)
    if event.private & (cuser.get_id() not in inviteeslist):
        response = make_response(jsonify(error='User can not contribute to this event.'), 403)
        return response
    con = Contribution(int(eventid), int(cuser.get_id()), float(amount))
    db.session.add(con)
    db.session.commit()
    response = make_response(jsonify(success='User contributed to this event.'), 200)
    return response
