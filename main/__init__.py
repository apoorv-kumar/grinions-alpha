from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_object('config')

# app.config['SERVER_NAME'] = 'localhost:8080' if os.environ.get('SERVER_NAME') is None else os.environ.get(
# 'SERVER_NAME')

db = SQLAlchemy(app)

from flask.ext.login import LoginManager

login_manager = LoginManager()
login_manager.init_app(app)

from main import views, models, api
