from datetime import datetime

from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
from main import db, app
from passlib.apps import custom_app_context as pwd_context


class User(db.Model):
    __tablename__ = 'users'

    _id = db.Column(db.Integer, primary_key=True, unique=True)
    email = db.Column(db.String(250), nullable=False, unique=True)
    password_hash = db.Column(db.String(250), nullable=False)
    first_name = db.Column(db.String(250), nullable=False)
    last_name = db.Column(db.String(250), nullable=False)
    gender = db.Column(db.String(1))
    picture = db.Column(db.String(250))  # fb profile link
    joined_date = db.Column(db.DateTime)
    link = db.Column(db.String(250))
    dob = db.Column(db.Date)
    google = db.Column(db.String(250))
    facebook = db.Column(db.String(250))
    phone_number = db.Column(db.String(15))
    country = db.Column(db.String(20))

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    @property
    def serialize(self):
        return {
            'id': self._id,
            'name': self.first_name + " " + self.last_name,
            'email': self.email,
        }

    def __init__(self, email, password, first_name, last_name, gender=None, picture=None, link=None, dob=None,
                 google=None, facebook=None, phone_number=None, country=None):
        self.email = email
        self.hash_password(password)  # hashing the password
        self.first_name = first_name
        self.last_name = last_name
        self.gender = gender
        self.picture = picture
        self.link = link
        self.dob = dob
        self.joined_date = datetime.utcnow()
        self.google = google
        self.facebook = facebook
        self.country = country
        self.phone_number = phone_number

    def __repr__(self):
        return '<User %r>' % self.email

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        try:
            return unicode(self._id)  # python 2
        except NameError:
            return str(self._id)  # python 3

    def generate_auth_token(self, expiration=600):
        s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None  # valid token, but expired
        except BadSignature:
            return None  # invalid token
        user = User.query.get(data['id'])
        return user


class Event(db.Model):
    __tablename__ = 'events'
    _id = db.Column(db.Integer, primary_key=True, unique=True)
    creator_id = db.Column(db.Integer, db.ForeignKey(User._id))
    event_name = db.Column(db.String(50), nullable=False)
    event_desc = db.Column(db.String(250))
    split_type = db.Column(db.Integer, nullable=False)
    contri_amt = db.Column(db.REAL, nullable=False)
    target_amt = db.Column(db.REAL, nullable=False)
    start_date = db.Column(db.Date, nullable=False)
    ends_by = db.Column(db.Date, nullable=False)
    private = db.Column(db.Boolean, nullable=False)
    picture = db.Column(db.String(250))

    def __init__(self, creator_id, event_name, event_desc, split_type, contri_amt=None, target_amt=None,
                 start_date=None, ends_by=None,
                 private=None, picture=None):
        self.creator_id = creator_id
        self.event_name = event_name
        self.event_desc = event_desc
        self.split_type = split_type
        self.contri_amt = contri_amt
        self.target_amt = target_amt
        self.start_date = start_date
        self.ends_by = ends_by
        self.private = private
        self.picture = picture

    @property
    def serialize(self):
        return {
            'id': self._id,
            'creator_id': self.creator_id,
            'event_name': self.event_name,
            'event_desc': self.event_desc,
            'split_type': self.split_type,
            'contri_amt': self.contri_amt,
            'target_amt': self.target_amt,
            'start_date': self.start_date,
            'ends_by': self.ends_by,
            'private': self.private,
            'picture': self.picture,
        }

    def get_id(self):
        try:
            return unicode(self._id)  # python 2
        except NameError:
            return str(self._id)  # python 3


class Contribution(db.Model):
    __tablename__ = 'contribution'
    _id = db.Column(db.Integer, primary_key=True, unique=True)
    event_id = db.Column(db.Integer, db.ForeignKey(Event._id))
    user_id = db.Column(db.Integer, db.ForeignKey(User._id))
    amount = db.Column(db.REAL, nullable=False)

    def __init__(self, event_id, user_id, amount):
        self.event_id = event_id
        self.user_id = user_id
        self.amount = amount

    @property
    def serialize(self):
        return {
            'id': self._id,
            'event_id': self.event_id,
            'user_id': self.user_id,
            'amount': self.amount,
        }

    def get_id(self):
        try:
            return unicode(self._id)  # python 2
        except NameError:
            return str(self._id)  # python 3


class Invitee(db.Model):
    __tablename__ = 'invitee_details'
    _id = db.Column(db.Integer, primary_key=True, unique=True)
    event_id = db.Column(db.Integer, db.ForeignKey(Event._id))
    email_id = db.Column(db.String(250), nullable=False)

    def __init__(self, event_id, email_id):
        self.event_id = event_id
        self.email_id = email_id

    @property
    def serialize(self):
        return {
            'id': self._id,
            'event_id': self.event_id,
            'user_id': self.email_id,
        }

    def get_id(self):
        try:
            return unicode(self._id)  # python 2
        except NameError:
            return str(self._id)  # python 3
