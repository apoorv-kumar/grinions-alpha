import os
import random
import string

from flask import render_template, send_from_directory, g, session as login_session
from flask import request, url_for, redirect
from flask.ext.login import logout_user, login_user, current_user, login_required
from main import app, db, login_manager
from main.models import User, Event, Invitee, Contribution
from utils import createUser, userInfoByEmail

login_manager.login_view = 'login'
login_manager.session_protection = 'strong'


# Flask-Login use this to reload the user object from the user ID stored in the session
@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.before_request
def before_request():
    g.user = current_user


############# VIEWS ################
# endpoint to reset the application
@app.route('/reset')
def resetapp():
    try:
        logout_user()
        login_session.clear()
    except:
        pass

    db.drop_all()
    db.create_all()
    return redirect(url_for('index'))


# endpoint to reset database
@app.route('/resetdb')
def resetdb():
    db.drop_all()
    db.create_all()
    return """Reset done</br>
        <a href= "/testdb">Test db</a>
    """


# endpoint to test database
@app.route("/testdb")
def testdb():
    u = User("admin@grinions.com", "admin", "Administrator", "", "M")
    try:
        db.session.add(u)
        return render_template('test_db.html', title='Success! DB working fine')
    except Exception as e:
        return render_template('test_db.html', title='Fail! Error in inserting object to DB')


# index view
@app.route('/index')
@app.route('/')
def index():
    nxt = request.args.get('next')
    print(nxt)
    if not g.user.is_authenticated:
        state = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(32))
        login_session['state'] = state
        return render_template('login.html', STATE=state, redir_url=nxt)
    return render_template('index.html', user=g.user)


@app.route('/logout')
def logout():
    logout_user()
    try:
        login_session.clear()
    except:
        pass
    return redirect(url_for('index'))


@app.route('/signup')
def signup():
    nxt = request.args.get('next')
    print(nxt)
    if not g.user.is_authenticated:
        state = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(32))
        login_session['state'] = state
        return render_template('signup.html', STATE=state, redir_url=nxt)
    # return render_template('index.html', user=g.user)
    return redirect(nxt or url_for('index'))


@app.route('/home')
@login_required
def home():
    # del login_session['logged_in']
    return redirect(url_for('index'))


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico',
                               mimetype='image/vnd.microsoft.icon')


@app.route('/test')
def test():
    return render_template('test.html')


# login view
@app.route('/login')
def login_view():
    nxt = request.args.get('next')
    if not g.user.is_authenticated:
        state = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(32))
        login_session['state'] = state
        return render_template('login.html', STATE=state, redir_url=nxt)
    return redirect(nxt or url_for('index'))


# VIEW and API for create password
@app.route('/create_password', methods=['POST', 'GET'])
def create_password():
    nxt = request.args.get('next')
    print(nxt)
    # if url was directly accessed without signing up
    if 'email' not in login_session:
        return redirect(url_for('index'))
    if userInfoByEmail(login_session['email']):
        return redirect(url_for('index'))
    if request.method == 'GET':
        return render_template('create_password.html')
    elif request.method == 'POST':
        password = str(request.form['new_password'])
        rpassword = str(request.form['conf_password'])
        if password != rpassword:
            return render_template('create_password.html', error="Passwords do not match.")
        else:
            user = createUser(login_session, password)
            login_user(user, remember=False)
            return redirect(nxt or url_for('index'))


# create event page
@app.route('/event/create')
@login_required
def view_create_event():
    return render_template('create_event.html')


# details of a particular event.
@app.route('/event/<int:eventid>')
def view_event(eventid):
    event = Event.query.filter_by(_id=eventid).first()
    if not event:
        return "Event not found"

    if (g.user is None) or (not g.user.is_authenticated):  # means anonymous user (not logged in)
        return render_template('event_details.html', event=event)
    else:
        userid = g.user.get_id()

        contri = Contribution.query.filter_by(event_id=eventid, user_id=g.user.get_id()).first()
        if not contri:

            return render_template('event_details.html', event=event, user=g.user)
        else:

            return render_template('event_details.html', event=event, user=g.user, contributed=contri)


# all events created by the logged in user
@app.route('/me/events')
@login_required
def get_all_events_of_user():
    events = Event.query.filter_by(creator_id=g.user.get_id()).all()
    return render_template('user_events.html', events=events, user=g.user)


# public events by an user
@app.route('/user/<int:userid>/events/all')
@login_required
def get_all_events_of_a_user(userid):
    events = Event.query.filter_by(creator_id=userid).all()
    return render_template('user_events.html', events=events, user=g.user)


# list of all users
@app.route('/users/all')
@login_required
def all_users():
    users = User.query.all()
    print(users)
    return render_template('all_users.html', users=users, cuser=g.user)


# public profile of an user
@app.route('/user/<int:userid>')
@login_required
def user_profile(userid):
    if userid == g.user.get_id():
        return redirect(url_for('me'))

    is_me = False

    user = User.query.filter_by(_id=userid).first()
    if not user:
        return render_template('profile_not_exist.html')
    if is_me:
        events = Event.query.filter_by(creator_id=userid).all()
    else:
        events = Event.query.filter_by(creator_id=userid, private=False).all()

    return render_template('profile.html', events=events, cuser=g.user, user=user, is_me=is_me)


# public profile of the currently logged in user
@app.route('/me')
@login_required
def me():
    # anon users will be redirected to login page
    user = g.user
    events = Event.query.filter_by(creator_id=user.get_id()).all()
    return render_template('profile.html', events=events, cuser=user, user=user, is_me=True)


@app.route('/event/<int:eventid>/contribute', methods=['POST'])
def contribute(eventid):
    canContribute = True
    cuser = g.user
    event = Event.query.filter_by(_id=eventid).first()
    invitees = Invitee.query.filter_by(event_id=event.get_id())
    inviteeslist = []
    for i in invitees:
        inviteeslist.append(i.email_id)
    if (event.private) & (cuser.get_id() not in inviteeslist):
        canContribute = False
        return render_template('contribute.html', canContribute=canContribute)

    return render_template('contribute.html', canContribute=canContribute)


# test page
@app.route('/grin_test')
def test_base():
    nxt = request.args.get('next')
    print(nxt)
    if not g.user.is_authenticated:
        state = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(32))
        login_session['state'] = state
        return render_template('login.html', STATE=state, redir_url=nxt)

    return render_template('test_grinions.html',
                           user=g.user,
                           user_pic="/static/images/apoorv.jpg",
                           page_title="User Home"
                           )
