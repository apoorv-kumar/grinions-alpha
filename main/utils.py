"""
@ All utility functions

"""
from main import db
from main.models import User

__author__ = 'Gangeshwar'


########### UTIL Functions #############
def createUser(login_session, password=None):
    email = login_session['email']
    fn = login_session['fname']
    ln = login_session['lname']
    try:
        pic = login_session['picture']
    except:
        pic = ''

    try:
        gender = login_session['gender']
        if gender == 'male':
            gender = 'M'
        elif gender == 'female':
            gender = 'F'
        else:
            gender = ''
    except:
        gender = ''

    newUser = User(email=email, first_name=fn, last_name=ln,
                   password=password, picture=pic,
                   gender=gender)
    db.session.add(newUser)
    db.session.commit()   # after commit, the _id column will have the id of the object in the table
    # updating the user profile link
    newUser.link = "/user/" + str(newUser._id)
    db.session.add(newUser)
    db.session.commit()
    print(newUser)  # debug
    return newUser


def userInfo(user_id):
    user = User.query.filter_by(_id=user_id).first()
    return user


def userInfoByEmail(email):
    user = User.query.filter_by(email=email).first()
    return user


def getUserId(email):
    try:
        user = User.query.filter_by(email=email).first()
        return user._id
    except:
        return None
